#include "Arduino.h"
#include "SparkFun_Tlc5940.h"
#include "dot_fade.h"

#define MAX_VALUE 4095

Dot_fade::Dot_fade(int i_speed, int i_attack, int i_decay, int i_sustain, int i_release, int i_channel_count) {
  speed = i_speed;
  attack = i_attack;
  decay = i_decay;
  sustain = i_sustain;
  release = i_release;
  channel_count = i_channel_count;
  // channel channels[i_channel_count];
  new_light_timer = 0;
};

void Dot_fade::step() {
  // Serial.println();
  // Serial.println("new_light_timer");
  // Serial.println(new_light_timer);
  if (new_light_timer == speed) {
    new_light_timer = 0;
    set_random_light();
  } else {
    new_light_timer++;
  }

  channel *channelp = channels;

  for (int i = 0; i < channel_count; i++) {
    channel *chanp = channelp + i;
    // Serial.println(i);
    // Serial.println(chanp->value);
    // Serial.println(chanp->count);
    // Serial.println(chanp->state);
    // Serial.println();

    if (chanp->state == ATTACK) {
      if (chanp->count == attack) {
        chanp->value = MAX_VALUE;
        chanp->state = SUSTAIN;
        chanp->count = 0;
      } else {
        chanp->value = ((long)chanp->count * MAX_VALUE) / attack;
        chanp->count++;
      }
    } else if (chanp->state == DECAY) {
      // // unused
      // if (channel.count === this.decay) {
      //   channel.value = MAX_VALUE;
      //   channel.state = SUSTAIN;
      //   channel.count = 0;
      // } else {
      //   channel.value = (channel.count * MAX_VALUE) / this.decay;
      //   channel.count++;
      // }
    } else if (chanp->state == SUSTAIN) {
      if (chanp->count == sustain) {
        chanp->state = RELEASE;
        chanp->count = 0;
      } else {
        chanp->count++;
      }
    } else if (chanp->state == RELEASE) {
      if (chanp->count == release) {
        chanp->value = 0;
        chanp->state = OFF;
        chanp->count = 0;
      } else {
        chanp->value = ((long)(release - chanp->count) * MAX_VALUE) / release;
        chanp->count++;
      }
    }

    Tlc.set(i, chanp->value);
  }

};

void Dot_fade::set_random_light() {
  Serial.println("set random");
  Serial.println(NUM_TLCS);
  channel *channelp = channels;
  for (int i = 0; i < channel_count; i++) {
    int random = rand() % channel_count;
    Serial.println(random);
    channel *chanp = channelp + random;
    // Serial.println(chanp->state);
    if (chanp->state == OFF) {
      chanp->state = ATTACK;
      // Serial.println(chanp->state);
      // Serial.println(channels[random].state);
      break;
    }
  }
}
