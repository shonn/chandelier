enum channel_state {
  OFF = 0,
  ATTACK,
  SUSTAIN,
  DECAY,
  RELEASE
};

struct channel {
  channel_state state;
  int count;
  long value;
};

class Dot_fade {
  int speed;
  int attack;
  int decay;
  int sustain;
  int release;
  int channel_count;
  int new_light_timer;
  channel channels[32];
  public:
    void step();
    void set_random_light();
    Dot_fade (
      int i_speed,
      int i_attack,
      int i_decay,
      int i_sustain,
      int i_release,
      int i_channel_count
    );
};
